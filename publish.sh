#!/bin/bash

# generate distribution packages
python setup.py sdist bdist_wheel

# upload package
twine upload dist/* --username ${USERNAME} --password ${PASSWORD}

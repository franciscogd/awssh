SETUP := python setup.py
VERSION := $(shell $(SETUP) --version)

build: clean
	help2man --name='AWSSH' --output='awssh.1' 'python awssh/awssh.py'
	pandoc --from=markdown --to=rst --output=README.rst README.md
	$(SETUP) sdist bdist_wheel

uninstall:
	echo y | pip uninstall ssh-aws || true

install: uninstall build
	pip install "`find dist -name 'ssh-aws*tar.gz'`"

pip-install: uninstall
	pip install ssh-aws --no-cache --user

sdist: clean
	$(SETUP) sdist bdist_wheel

release_test: build
	git tag "$(VERSION)"
	git push --tags
	python -m twine upload --username ${TWINEUSER} --password ${PASSWORD} --repository-url https://test.pypi.org/legacy/ dist/*

publish: build
	git tag "$(VERSION)"
	git push bitbucket --tags

clean:
	rm -rf build dist *.egg-info
